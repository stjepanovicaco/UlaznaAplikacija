#include "Authenticate.h"

bool authenticate(const std::string &username, const std::string &password) {
	std::ifstream file("userdb.txt");
	std::string fusername, fpassword;

	while (file) {
		 std::getline(file, fusername, ';'); 
	   	 std::getline(file, fpassword);

		if (fusername == username && fpassword == password)
			return true;
	}

	return false;
}