#include "MainMenu.h"
using std::cout;
using std::cin;
using std::endl;

MainMenu* MainMenu::instance = nullptr;

void MainMenu::start() {
	while (active) {
		system("cls");
		drawLogo();
		cout << "                                           " <<Vrijeme::getCurrentTime();
		izbor();
	}
}

void MainMenu::izbor()
{
		system("color 3A");
		std::string input;
		cout << "                       Dobrodosli u glavni meni!" << endl;
		cout << "                       Izaberite opciju: " << endl << endl;
		cout << "                             Prijava [P]" << endl;
//		cout << "                         Podesavanja [S]" << endl;
		cout << "                             Pomoc   [H]" << endl;
		cout << "                             IZLAZ [0]" << endl;
		do 
		{
			cin >> input;
			
		} while (input < "A" && input > "Z" && sizeof(input) != 1);

		if (input == "P")
		{	
			bool loginSuccess = login();
			if (loginSuccess == true)
			{
				WorkerMenu::getInstance()->start();
				return;
			}
			else if (loginSuccess == false)
				return;
			
		}
		else if (input == "H")
		{
			system("cls");
			cout << "Dobrodosli u ulaznu aplikaciju paketa HWTH za rad sa autoputevima."<<endl;
			cout << "Ovaj program je dio paketa iz softvera za autoputeve zaduzen za unos automobila u bazu podataka " << endl<<endl;
			cout << "Aplikaciju radili: \n Aleksandar Stjepanovic \n Nemanja Strkic \n Nikola Azaric \n Relja Beslac" << endl;
			cout << endl << endl << "      Stisnite bilo koje dugme za povratak nazad";
			_getch();
			return;
		}
		else if (input == "0")
		{
			setActive(false);
			return;
		}

		else
		{
			std::cout << endl << endl << "Neispravan unos!"<<endl;
			std::cout << "Stisni bilo koje dugme" << endl <<endl <<endl;
			_getch();
			return;
		}
		return;
}


