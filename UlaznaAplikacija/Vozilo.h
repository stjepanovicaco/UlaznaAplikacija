#pragma once
#include "standard_headers.h"
#include "Vrijeme.h"
class Vozilo
{
	std::string tablice;
	std::string kategorija;
	int ulaz;
public:
	Vozilo(std::string __tablice, std::string __kategorija, int __ulaz) : tablice(__tablice), kategorija(__kategorija), ulaz(__ulaz) {};
	std::string generateInputCSV();
	Vozilo();
};

