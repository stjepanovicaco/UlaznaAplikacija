#include "Vrijeme.h"

std::string Vrijeme::getCurrentTime()
{
	time_t now = time(0);
	char* dt = ctime(&now);
	return dt;
}

/*
	sledeca metoda je potrebna za generisanje izvjestaja i njegovo formatiranje kako bi se hvatao u izlaznoj aplikaciji
*/

std::string Vrijeme::getCurrentTimeNumerical()
{
	std::string toConvert = Vrijeme::getCurrentTime();
	std::string toReturn = "";
	std::string arrMonth[] = { "Jan","Feb","Mar","Apr","Jun","Jul","Aug","Sep","Oct","Nov","Dec" };
	std::string arrNum[] = { "1","2","3","4","5","6","7","8","9","10","11","12" };

	std::string sub[6];
	std::string danUSedmici, dan, mjesec, godina, vrijeme;
	for (int i = 0, j = 0; i<toConvert.length(); i++)
	{
		if (toConvert[i] == ' ' || toConvert[i] == '\n')
			i++, j++;
		sub[j].push_back(toConvert[i]);
	}
	danUSedmici = sub[0];
	mjesec = sub[1];
	dan = sub[2];
	vrijeme = sub[3];
	godina = sub[4];
	for (int i = 0; i < 11; i++)
		if (mjesec == arrMonth[i])
			mjesec = arrNum[i+1];
	toReturn = dan + "." + mjesec + "." + godina +"." + "," + vrijeme;
	return toReturn;
}
