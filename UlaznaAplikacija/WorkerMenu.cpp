#include "WorkerMenu.h"
using std::cout;
using std::cin;
using std::endl;
WorkerMenu* WorkerMenu::instance = nullptr;

void WorkerMenu::start() {
	while (active) {
		system("cls");
		WorkerMenu::izbor();
	}
}

void WorkerMenu::izbor()
{
		char input;
		cout << "                       Dobrodosli u radnicki meni!" << endl;
		cout << "Izaberite opciju: " << endl;
		cout << "   Unos [U]" << endl;
		cout << "  Pomoc [P]" << endl;
		cout << "   IZLAZ [0]" << endl;
		cout << endl << endl;
		cout << "                                            " << Vrijeme::getCurrentTime();
		cin >> input;

		if (input == 'U') {
			unos();
		}
		else if (input == 'P') {
			pomoc();
			return;
		}
		else if (input == '0') {
			WorkerMenu::setActive(false);		
			return;
		}
}

void WorkerMenu::pomoc()
{
	system("cls");
	std::cout << "Unosi se tablica i kategorija. Vrijeme ulaska automobila se generise automatski" << endl;
	std::cout << std::endl << std::endl << "Stisnite bilo koje dugme za pocetak.....";
	_getch();
}

void WorkerMenu::unos()
{
	std::string temp_tablica;
	std::string temp_kategorija;
	std::cout << "Tablica: ";
	std::cin >> temp_tablica;
	std::cout << endl;
	std::cout << "Kategorija: ";
	std::cin >> temp_kategorija;
	std::cout << endl;

	/*
		sledeci kod sluzi za generisanje slucajnog broja u opsegu od 1 do 5 sto simulira razlicite 
		ulazne kucice 
	*/

	std::random_device rd; // obtain a random number from hardware
	std::mt19937 eng(rd()); // seed the generator
	std::uniform_int_distribution<> distr(1, 5); // define the range
	int ulaz = distr(eng);
	toFile(Vozilo(temp_tablica, temp_kategorija,ulaz));
}

bool WorkerMenu::toFile(Vozilo __vozilo)
{
	std::ofstream output("log.txt", std::ios_base::app);
	if (!output)
		return false;
	std::string element = __vozilo.generateInputCSV();
	output << element;
	return true;
}
