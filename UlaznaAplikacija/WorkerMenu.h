#pragma once
#include "login.h"
#include "Draw.h"
#include "Vrijeme.h"
#include "Vozilo.h"
class WorkerMenu { //singleton, obezbjedjivanje samo jednog menija u programu
public:
	static WorkerMenu* getInstance() {
		if (instance == nullptr)
			instance = new WorkerMenu();
		return instance;
	}

	void start();
	void izbor();
	void pomoc();
	void unos();
	bool toFile(Vozilo);
	void setActive(bool active) { this->active = active; }
	bool isActive() const { return this->active; }
private:
	WorkerMenu() = default;
	WorkerMenu(WorkerMenu&) = delete;
	WorkerMenu(WorkerMenu&&) = delete;
	WorkerMenu& operator=(WorkerMenu&) = delete;
	WorkerMenu& operator=(WorkerMenu&&) = delete;

	static WorkerMenu* instance;

	bool active = true;
};
