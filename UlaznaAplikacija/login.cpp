#include "login.h"
#include "MainMenu.h"
#include "WorkerMenu.h"
#include "Authenticate.h"
using std::string;
using std::cout;
using std::endl;

string getUsername() {
	string username;
	cout << endl << endl << endl;
	cout << "Unesite korisnicko ime: (aco)" << endl;
	std::cin >> username;
	return username;
}

string getPassword() {
	string pass;
	char ch;
	cout << "Unesite lozinku: (coa123)" << endl;
	while (1)
	{
		ch = _getch();
		if (ch == 13)
			break;
		pass.push_back(ch);
		if (ch == 8)
			cout << "\b \b";
		if (ch != 8) {
			cout << "*";
		}
	}//zamjena karaktera zvijezdom prilikom unosenja sifre
	return pass;
}

bool login() 
{
	string username = getUsername();
	string pass = getPassword();
	cout << endl;
	bool auth = authenticate(username, pass);
	if (auth == true) 
	{
		cout << endl << "Uspjesna prijava!" << endl;
		cout << "Stisnite bilo koje dugme za pocetak....";
		_getch();
		return true;
	}
	else if (auth == false)
	{
		cout << endl << endl << "Neuspjesna prijava" << endl;
		cout << "Stisnite bilo koje dugme za povratak" << endl;
		_getch();
		return false;
	}
	
	else return false;
}

